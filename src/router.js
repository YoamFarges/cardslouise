import Vue from "vue";
import Router from "vue-router";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Tabou from "./views/Tabou.vue";
import Pronoms from "./views/Pronoms.vue";
import Conditionnel from "./views/Conditionnel.vue";
import Conséquence from "./views/Consequence.vue";

Vue.use(Router);

export default new Router({
routes: [
    {
        path: "/",
        name: "tabou",
        components: {
            header: Header,
            default: Tabou,
            footer: Footer
        }
    },
    {
        path: "/pronoms",
        name: "pronoms",
        components: {
            header: Header,
            default: Pronoms,
            footer: Footer
        }
    },
    {
        path: "/conditionnel",
        name: "conditionnel",
        components: {
            header: Header,
            default: Conditionnel,
            footer: Footer
        }
    },
    {
        path: "/consequence",
        name: "consequence",
        components: {
            header: Header,
            default: Conséquence,
            footer: Footer
        }
    }
        ]

});